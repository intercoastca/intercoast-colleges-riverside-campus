The central mission of InterCoast Colleges is to provide associates degrees and certificate programs for careers in allied health, business, and skilled trade industries and prepare students to meet employer expectations for entry level employment.

Address: 1989 Atlanta Ave, Riverside, CA 92507

Phone: 951-779-1300